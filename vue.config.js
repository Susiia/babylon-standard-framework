/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-06-23 14:02:45
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-20 10:31:50
 */
const { defineConfig } = require('@vue/cli-service')
const buildPath = '/labPage/photoWallDemoPage/' // 博客路径
module.exports = defineConfig({
  // 打包目录
  publicPath: process.env.NODE_ENV === 'production' ? buildPath : './',
  // 是否生成SourceMap
  productionSourceMap: process.env.NODE_ENV !== 'production',
  // 样式部分的设置
  css: {
    // 是否生成SourceMap
    sourceMap: process.env.NODE_ENV !== 'production'
  },
  // 这玩应为true就可以用template标签
  transpileDependencies: true,
  // 开发服务器
  devServer: {
    // 开启https
    server: 'https'
  }
})
