import { Scene } from '@babylonjs/core'

export default function
debuggerLayer (scene:Scene, customKey?:string) {
  window.addEventListener('keydown', (ev) => {
    if (ev.shiftKey && ev.ctrlKey && ev.altKey && ev.key === (customKey || 'I')) {
      if (scene.debugLayer.isVisible()) {
        scene.debugLayer.hide()
      } else {
        scene.debugLayer.show(
          { embedMode: true }
        )
      }
    }
  })
}
