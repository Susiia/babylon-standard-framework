/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-04 09:39:52
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-14 15:02:59
 */
// 播放所有动画
import { AnimationGroup, AnimationPropertiesOverride, AssetContainer } from '@babylonjs/core'

export default function
modelAnimation (assetContainer : AssetContainer) {
  const overrides = new AnimationPropertiesOverride()
  overrides.enableBlending = true
  overrides.blendingSpeed = 0.5
  assetContainer.skeletons.forEach((r) => {
    r.animationPropertiesOverride = overrides
  })
  assetContainer.animationGroups.forEach((r, i) => {
    if (i === 0) {
      r.play(true)
      r.speedRatio = -1
      r.goToFrame(0)
      r.loopAnimation = false
      r.setWeightForAllAnimatables(1)
    } else {
      const animationMixTemp = AnimationGroup.MakeAnimationAdditive(r)
      animationMixTemp.play(true)
      animationMixTemp.setWeightForAllAnimatables(1)
    }
  })
}
