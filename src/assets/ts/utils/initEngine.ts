import { Color4, Engine, EquiRectangularCubeTexture, Scene, Vector3 } from '@babylonjs/core'

export default function
initEngine (canvas: HTMLCanvasElement, envTexUrl?:string, clearColor?:Color4) {
  // 初始化引擎
  const engine = new Engine(
    canvas,
    true,
    {},
    true
  )
  // 初始化场景
  const scene = new Scene(engine)// 初始化场景
  scene.clearColor = clearColor || new Color4(0, 0, 0, 0) // 清除色
  // 环境贴图
  if (envTexUrl) {
    const envTexture = new EquiRectangularCubeTexture(
      envTexUrl,
      scene,
      1024
    )
    scene.createDefaultEnvironment({
      createGround: false,
      createSkybox: false,
      environmentTexture: envTexture
    })
  }
  // 基础场景物理
  const assumedFramesPerSecond = 60
  const earthGravity = -9.34
  scene.gravity = new Vector3(0, earthGravity / assumedFramesPerSecond, 0) // 设置场景重力(模拟地球)
  scene.collisionsEnabled = true // 开启碰撞检测
  RenderLoop(engine, scene)// 执行渲染循环
  return { engine, scene }
}

function RenderLoop (engine:Engine, scene:Scene):void {
  // 添加窗口变化事件监听
  window.addEventListener('resize', () => {
    engine.resize()
  })
  // 执行循环
  engine.runRenderLoop(() => {
    scene.render()
  })
}
