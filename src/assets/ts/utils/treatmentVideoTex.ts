// 模型处理部件 视频贴图处理
import { AbstractMesh, Scene, StandardMaterial, VideoTexture } from '@babylonjs/core'

export default function
treatmentVideoTex (url:string, mesh:AbstractMesh, scene:Scene) {
  // videoMaterial
  const videoMat = new StandardMaterial('videoMat', scene)
  // videoTexture
  const videoTex = new VideoTexture('videoTex', url, scene)
  videoMat.diffuseTexture = videoTex
  videoMat.emissiveTexture = videoTex
  mesh.material = videoMat
}
