/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-13 10:51:36
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-13 10:55:44
 */
import { Client } from 'colyseus.js'

const Clientserver = new Client('ws://localhost:2567')

Clientserver.joinOrCreate('room1').then(room => {
  console.log('已连接房间')
  console.log(room)
}).catch(err => {
  console.log('连接失败')
  console.log(err)
})
