import { Scene, ArcRotateCamera } from '@babylonjs/core'

// 滚轮线性相机
export default class WheelLinearCamera {
    private scene:Scene
    private camera!:ArcRotateCamera
    constructor (scene:Scene) {
      this.scene = scene
      this.initCamera()
    }

    private initCamera () {
      // this.camera = new ArcRotateCamera('camera')
    }
}
