import { Scene, UniversalCamera, Vector3, Viewport } from '@babylonjs/core'

// 环形屏幕相机
export default class AnnularCamera {
  private _name: string // 相机名
  private readonly scene:Scene // 场景
  private readonly mainCamera:UniversalCamera // 主相机
  private deputyCameras:UniversalCamera[] = [] // 副相机组
  private readonly CameraCount:number // 相机数量
  private _position:Vector3 // 相机位置
  private _ellipsoid!:Vector3 // 碰撞检测盒子大小
  private _speed!: number // 相机速度
  private _checkCollisions!: boolean // 检查碰撞检测
private _applyGravity!: boolean // 应用重力
constructor (
  name:string, // 相机名
  position:Vector3, // 位置
  scene:Scene, // 场景
  CameraCount?:number // 相机数量
) {
  // 初始化相机名
  this._name = name
  // 初始化相机位置
  this._position = position
  // 初始化相机数量
  this.CameraCount = (CameraCount || 4) < 4 ? 4 : (CameraCount || 4)
  // 初始化场景
  this.scene = scene
  // 初始化主相机
  this.mainCamera = new UniversalCamera(
    this._name,
    this.position,
    this.scene
  )
  this.mainCamera.fov = Math.PI / (this.CameraCount / 2) // 设置主相机视野
  this.mainCamera.fovMode = UniversalCamera.FOVMODE_HORIZONTAL_FIXED // 设置主相机视野模式
  // eslint-disable-next-line no-unused-expressions
  scene.activeCameras?.push(this.mainCamera) // 主相机推入场景激活相机组
  this.mainCamera.viewport = new Viewport(0, 0, 1 / this.CameraCount, 1) // 设置主相机视口
  // 初始化副相机组
  for (let i = 0; i < this.CameraCount - 1; i++) {
    // 定义副相机
    const deputyCamera = new UniversalCamera(
      this._name + 'Camera' + (i + 1),
      new Vector3(0, 0, 0),
      this.scene
    )
    deputyCamera.parent = this.mainCamera // 设置父级别为主相机
    this.deputyCameras.push(deputyCamera) // 推入副相机组
    // eslint-disable-next-line no-unused-expressions
    scene.activeCameras?.push(deputyCamera) // 副相机推入场景激活相机组
    deputyCamera.rotation = new Vector3(0, (Math.PI / (this.CameraCount / 2)) * (i + 1), 0) // 设置副相机旋转
    deputyCamera.fov = Math.PI / (this.CameraCount / 2) // 设置相机视野
    deputyCamera.fovMode = UniversalCamera.FOVMODE_HORIZONTAL_FIXED // 设置相机视野模式
    deputyCamera.viewport = new Viewport((1 / this.CameraCount) * (i + 1), 0, 1 / this.CameraCount, 1) // 设置相机视口
  }
}

  public attachControl = (canvas: HTMLCanvasElement, noPreventDefault?: boolean | undefined):void => {
    this.mainCamera.attachControl(canvas, noPreventDefault)
  }

  /*
  * getter/setter
  */

  // 碰撞检测盒getter/setter
  get ellipsoid (): Vector3 {
    return this._ellipsoid
  }

  set ellipsoid (value: Vector3) {
    this.mainCamera.ellipsoid = value
    this._ellipsoid = value
  }

  // 相机位置getter/setter
  get position (): Vector3 {
    return this._position
  }

  set position (value: Vector3) {
    this.mainCamera.position = value
    this._position = value
  }

  // 相机名getter/setter
  public get name (): string {
    return this._name
  }

  public set name (value: string) {
    this.mainCamera.name = value
    this._name = value
  }

  // 相机速度
  public get speed (): number {
    return this._speed
  }

  public set speed (value: number) {
    this.mainCamera.speed = value
    this._speed = value
  }

  // 是否应用碰撞检测
  public get checkCollisions (): boolean {
    return this._checkCollisions
  }

  public set checkCollisions (value: boolean) {
    this.mainCamera.checkCollisions = value
    this._checkCollisions = value
  }

  // 是否应用重力
  public get applyGravity (): boolean {
    return this._applyGravity
  }

  public set applyGravity (value: boolean) {
    this.mainCamera.applyGravity = value
    this._applyGravity = value
  }
}
