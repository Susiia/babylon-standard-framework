/*
 * @Descripttion:在线文件用的照片墙
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-06-28 08:38:10
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-20 09:46:13
 */

import { ArcRotateCamera, ArcRotateCameraPointersInput, Color3, Mesh, StandardMaterial, MeshBuilder, PBRMaterial, Scene, Texture, UniversalCamera, Vector3, ISize, VideoTexture, Color4, DefaultRenderingPipeline, Engine } from '@babylonjs/core'
import '@babylonjs/inspector'
import TWEEN from '@tweenjs/tween.js'
export type fileItem={
  url:string, // 文件路径
  content:string, // 鼠标悬浮内容
  // isClick为true的时候需要添加效果（可点击）
  isClick:boolean, // 是否可点击
}
// 文件列表
export type fileList ={
  [key:string]:fileItem
}
// 照片墙
export default class PhotoWall {
  private canvas!:HTMLCanvasElement // canvas
  private engine!:Engine // Engine
  private scene!:Scene // Scene
  private camera!:ArcRotateCamera // 相机
  private cameraTarget!:Vector3 // 相机目标
  private photoJson!:fileList// 照片列表
  private videoList!:Map<string, HTMLVideoElement> // 视频列表
  private hoverContent!:HTMLDivElement // 悬浮内容
  private effectBoxVideoUrl:string // 特效视频地址
  private clickInteraction!:(id:string) => void // 交互回调
  /**
   * @Descripttion:
   * @Author: 刘译蓬
   * @msg:
   * @param {HTMLCanvasElement} canvas
   * @param {fileList} 文件列表
   * @param {string} 特效视频
   * @return {*}
   */
  constructor (canvas:HTMLCanvasElement, fileList:fileList, effectBoxVideoUrl:string) {
    this.canvas = canvas // 赋值canvas
    this.photoJson = fileList // 初始化photojson
    this.videoList = new Map() // 初始化videoLIst
    this.effectBoxVideoUrl = effectBoxVideoUrl
    this.initEngine(canvas)
    this.initCamera() // 初始化相机
    this.makeHoverContent() // 初始化悬浮板
    this.initInteraction() // 初始化交互事件
    this.initPostProcess() // 初始化后处理
    this.makePhotoCloud(this.photoJson) // 生成照片云
    this.scene.registerAfterRender(() => { TWEEN.update() }) // 循环更新控制器
  }

  /**
   * @Descripttion:初始化引擎
   * @Author: 刘译蓬
   * @msg:
   * @param {HTMLCanvasElement} canvas
   * @return {*}
   */
  private async initEngine (canvas: HTMLCanvasElement): Promise<void> {
    this.canvas = canvas // 初始化canvas
    this.engine = new Engine(
      this.canvas,
      true,
      {},
      true
    )
    this.scene = new Scene(this.engine)// 初始化场景
    this.scene.clearColor = new Color4(0, 0, 0, 0)
    const assumedFramesPerSecond = 60
    const earthGravity = -9.34
    this.scene.gravity = new Vector3(0, earthGravity / assumedFramesPerSecond, 0) // 设置场景重力(模拟地球)
    this.scene.collisionsEnabled = true // 开启碰撞检测
    this.RenderLoop()// 执行渲染循环
  }

  /**
   * @Descripttion:渲染循环
   * @Author: 刘译蓬
   * @msg:
   * @return {*}
   */
  private RenderLoop ():void {
    // 添加窗口变化事件监听
    window.addEventListener('resize', () => {
      this.engine.resize()
    })
    // 执行循环
    this.engine.runRenderLoop(() => {
      this.scene.render()
    })
  }

  /**
   * @Descripttion: 初始化后处理
   * @Author: 刘译蓬
   * @msg:
   * @param {UniversalCamera} camera
   * @param {Scene} scene
   * @return {*}
   */
  private initPostProcess (camera?:UniversalCamera|ArcRotateCamera, scene?:Scene):void {
    const defaultPipeline = new DefaultRenderingPipeline('default', true, scene || this.scene, [camera || this.camera])

    // glowLayer光晕 兼容ssao2、运动模糊
    defaultPipeline.glowLayerEnabled = true
    if (defaultPipeline.glowLayer) {
      defaultPipeline.glowLayer.blurKernelSize = 16 // 16 by default
      defaultPipeline.glowLayer.intensity = 0.3 // 1 by default
    }

    // 景深
    // defaultPipeline.depthOfFieldEnabled = true
    // defaultPipeline.depthOfFieldBlurLevel = 0 // 0 by default
    // defaultPipeline.depthOfField.fStop = 1.4 // 1.4 by default
    // defaultPipeline.depthOfField.focalLength = 50 // 50 by default, mm
    // defaultPipeline.depthOfField.focusDistance = 2000 // 2000 by default, mm
    // defaultPipeline.depthOfField.lensSize = 50 // 50 by default

    // 电影颗粒
    defaultPipeline.grainEnabled = true
    defaultPipeline.grain.animated = true
    defaultPipeline.grain.intensity = 5

    // 图片后处理
    defaultPipeline.imageProcessing.contrast = 1.5
    defaultPipeline.imageProcessing.vignetteEnabled = true

    // 清晰边缘
    defaultPipeline.sharpenEnabled = true
    defaultPipeline.sharpen.edgeAmount = 0.5
  }

  /**
   * @Descripttion: 初始化旋转相机
   * @Author: 刘译蓬
   * @msg:
   * @return {*}
   */
  private initCamera (): void {
    this.cameraTarget = new Vector3(0, 0, 0)
    this.camera = new ArcRotateCamera('ArcRotateCamera', 0, Math.PI / 2, 3 * Math.log(Object.keys(this.photoJson).length) + 5, this.cameraTarget, this.scene)
    this.scene.activeCamera = this.camera
    this.camera.minZ = 1.4
    this.camera.fovMode = ArcRotateCamera.FOVMODE_VERTICAL_FIXED
    // 相机控制
    this.camera.inputs.clear()
    const inputs = new ArcRotateCameraPointersInput()
    inputs.angularSensibilityX = 5000
    inputs.angularSensibilityY = 5000
    inputs.panningSensibility = 5000
    inputs.pinchDeltaPercentage = 0.0001
    inputs.pinchInwards = false
    this.camera.inputs.add(inputs)
    this.camera.inputs.addMouseWheel()
    this.camera.attachControl(this.canvas, true)
    // 相机行为
    this.camera.inertia = 0.98
    this.camera.panningInertia = 0.98
    this.camera.lowerRadiusLimit = 1
    this.camera.upperRadiusLimit = 3 * Math.log(Object.keys(this.photoJson).length) + 20
    this.camera.wheelDeltaPercentage = 0.0001
    this.camera.zoomToMouseLocation = true
    this.camera.useAutoRotationBehavior = true
    this.camera.autoRotationBehavior!.idleRotationWaitTime = 0
    this.camera.autoRotationBehavior!.idleRotationSpinupTime = 10
    this.camera.speed = 0
  }

  /**
   * @Descripttion: 交互
   * @Author: 刘译蓬
   * @msg:
   * @return {*}
   */
  private initInteraction () {
    // 交互动作
    let currentFile = ''
    this.scene.onPointerObservable.add(e => {
      // 点空白
      if (e.type === 32) {
        if (!e.pickInfo!.pickedMesh) {
          this.fadeAnimation(this.camera.target, { x: 0, y: 0, z: 0 })
          const radiusTemp = {
            _radius: this.camera.radius,
            _camera: this.camera,
            get radius () {
              return this._radius
            },
            set radius (value) {
              this._camera.radius = value
              this._radius = value
            }
          }
          this.fadeAnimation(radiusTemp, { radius: 3 * Math.log(Object.keys(this.photoJson).length) + 5 })
          this.videoList.forEach(r => {
            r.pause()
            r.currentTime = r.duration / 2
            r.muted = true
          })
        }
      }
      // 点到mesh
      if (e.type === 16) {
        // 如果mesh是交互板的话，交互执行
        if (this.photoJson[e.pickInfo!.pickedMesh!.name].isClick) {
          this.clickInteraction(e.pickInfo!.pickedMesh!.name)
        }
        // 相机目标动画
        this.fadeAnimation(this.camera.target, e.pickInfo!.pickedMesh!.position)
        // 镜头旋转半径缓存
        const radiusTemp = {
          _radius: this.camera.radius,
          _camera: this.camera,
          get radius () {
            return this._radius
          },
          set radius (value) {
            this._camera.radius = value
            this._radius = value
          }
        }
        // 旋转半径动画
        this.fadeAnimation(radiusTemp, { radius: 2.5 })
        // 如果是视频的话就解除静音
        if (e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name && e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name.indexOf('videoTexture') !== -1 && e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name !== currentFile) {
          if (currentFile.indexOf('videoTexture') !== -1) {
            this.videoList.get(currentFile)?.pause()
            this.videoList.get(currentFile)!.currentTime = this.videoList.get(currentFile)!.duration / 2
            this.videoList.get(currentFile)!.muted = true
          }
          currentFile = e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name
          const video = this.videoList.get(e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name)!
          video.muted = false
          video.currentTime = 0
          video.play()
        }
      }
      // 移动
      if (e.type === 4) {
        if (e.pickInfo?.hit) {
          this.hoverContent.style.top = (20 + e.event.clientY).toString() + 'px'
          this.hoverContent.style.left = (20 + e.event.clientX).toString() + 'px'
          this.hoverContent.style.display = 'block'
          this.hoverContent.innerHTML = e.pickInfo.pickedMesh?.name ? this.photoJson[e.pickInfo.pickedMesh.name].content : ''
          if (this.photoJson[e.pickInfo.pickedMesh!.name].isClick) {
            this.canvas.style.cursor = 'pointer'
          }
        } else {
          this.hoverContent.style.display = 'none'
        }
      }
    })
  }

  /**
   * @Descripttion: 交互监听暴露
   * @Author: 刘译蓬
   * @msg:
   * @param {function} clickInteraction
   * @return {*}
   */
  public interactionOnClick (clickInteraction:(id:string) => void) {
    this.clickInteraction = clickInteraction
  }

  /**
   * @Descripttion:// 悬浮框
   * @Author: 刘译蓬
   * @msg:
   * @return {*}
   */
  private makeHoverContent () {
    this.hoverContent = document.createElement('div')
    this.hoverContent.innerText = '123'
    this.hoverContent.style.position = 'absolute'
    this.hoverContent.style.zIndex = '20'
    this.hoverContent.style.pointerEvents = 'none'
    document.getElementsByTagName('body')[0].appendChild(this.hoverContent)
  }

  /**
   * @Descripttion: // 随机生成照片散点
   * @Author: 刘译蓬
   * @msg:
   * @param {fileList} photoList
   * @return {*}
   */
  makePhotoCloud (photoList:fileList):void {
    for (const i in photoList) {
      if (Object.prototype.hasOwnProperty.call(photoList, i)) {
        const r = photoList[i]
        if (r.url.indexOf('.mp4') !== -1) {
          // 视频
          // texture
          const videoTextureTemp = new VideoTexture('videoTexture' + i, r.url, this.scene, false, false, VideoTexture.TRILINEAR_SAMPLINGMODE,
            {
              autoPlay: false,
              muted: true,
              autoUpdateTexture: true
            }
          )
          this.videoList.set(videoTextureTemp.name, videoTextureTemp.video)
          videoTextureTemp.onLoadObservable.add(texture => {
            const textureSize = texture.getSize()
            // material
            const videoMaterialTemp = new PBRMaterial('videoMaterial' + i, this.scene)
            videoMaterialTemp.albedoTexture = videoTextureTemp
            videoMaterialTemp.emissiveTexture = videoTextureTemp
            videoMaterialTemp.emissiveColor = new Color3(1, 1, 1)
            videoMaterialTemp.metallic = 0
            videoMaterialTemp.roughness = 0
            // mesh
            const videoPlanTemp = MeshBuilder.CreatePlane(i, this.meshSize(textureSize))
            videoPlanTemp.enablePointerMoveEvents = true
            videoPlanTemp.material = videoMaterialTemp
            videoPlanTemp.billboardMode = Mesh.BILLBOARDMODE_ALL
            // position
            videoPlanTemp.position = new Vector3(
              this.planeRange(Object.keys(photoList).length),
              this.planeRange(Object.keys(photoList).length),
              this.planeRange(Object.keys(photoList).length)
            )
            if (r.isClick) {
              this.makeEffectsBox(videoPlanTemp.name, videoPlanTemp.position, this.meshSize(textureSize))
            }
          })
        } else if (r.url.indexOf('.png') !== -1 || r.url.indexOf('.jpg') !== -1) {
          // 图片
          // texture
          const photoTextureTemp = new Texture(r.url, this.scene)
          photoTextureTemp.onLoadObservable.add(texture => {
            const textureSize = texture.getSize()
            // material
            const photoMaterialTemp = new PBRMaterial('photoMaterial' + i, this.scene)
            photoMaterialTemp.albedoTexture = photoTextureTemp
            photoMaterialTemp.emissiveTexture = photoMaterialTemp.albedoTexture
            photoMaterialTemp.emissiveColor = new Color3(1, 1, 1)
            photoMaterialTemp.metallic = 0
            photoMaterialTemp.roughness = 0
            // mesh
            const photoPlanTemp = MeshBuilder.CreatePlane(i, this.meshSize(textureSize))
            photoPlanTemp.enablePointerMoveEvents = true
            photoPlanTemp.material = photoMaterialTemp
            photoPlanTemp.billboardMode = Mesh.BILLBOARDMODE_ALL
            // position
            photoPlanTemp.position = new Vector3(
              this.planeRange(Object.keys(photoList).length),
              this.planeRange(Object.keys(photoList).length),
              this.planeRange(Object.keys(photoList).length)
            )
            if (r.isClick) {
              this.makeEffectsBox(photoPlanTemp.name, photoPlanTemp.position, this.meshSize(textureSize))
            }
          })
        }
      }
    }
  }

  /**
   * @Descripttion: // 特效盒子
   * @Author: 刘译蓬
   * @msg:
   * @param {string} name
   * @param {Vector3} position
   * @param {ISize} size
   * @return {*}
   */
  private makeEffectsBox (name:string, position:Vector3, size:ISize) {
    // mesh
    const effectBoxTemp = MeshBuilder.CreateBox(name, { width: size.width + 3, height: size.height + 3, depth: 0.01 }, this.scene)
    effectBoxTemp.position = position
    effectBoxTemp.billboardMode = Mesh.BILLBOARDMODE_ALL
    effectBoxTemp.isPickable = false
    // texture
    const videoTextureTemp = new VideoTexture(name, this.effectBoxVideoUrl, this.scene, false, false, VideoTexture.TRILINEAR_SAMPLINGMODE,
      {
        autoPlay: true,
        muted: true,
        autoUpdateTexture: true
      }
    )
    // material
    const MaterialTemp = new StandardMaterial(name, this.scene)
    MaterialTemp.diffuseTexture = videoTextureTemp
    MaterialTemp.emissiveTexture = MaterialTemp.diffuseTexture
    MaterialTemp.ambientColor = Color3.White()
    MaterialTemp.alpha = 0.5
    MaterialTemp.alphaMode = Engine.ALPHA_ADD
    MaterialTemp.emissiveColor = Color3.White()
    effectBoxTemp.material = MaterialTemp
  }

  /**
   * @Descripttion: // 过渡动画方法
   * @Author: 刘译蓬
   * @msg:
   * @param {any} target
   * @param {any} to
   * @return {*}
   */
  private fadeAnimation (target: any, to: any) {
    const tw = new TWEEN.Tween(target).to(to, 1000)
    tw.easing(TWEEN.Easing.Cubic.InOut)//
    tw.start()
  }

  /**
   * @Descripttion: // 片大小
   * @Author: 刘译蓬
   * @msg:
   * @param {ISize} textureSize
   * @return {*}
   */
  private meshSize (textureSize: ISize) {
    return textureSize.height > textureSize.width ? { height: 2, width: (textureSize.width / textureSize.height) * 2 } : { height: (textureSize.height / textureSize.width) * 2, width: 2 }
  }

  /**
   * @Descripttion: // 范围
   * @Author: 刘译蓬
   * @msg:
   * @param {number} length
   * @return {*}
   */
  private planeRange (length:number): number {
    return (Math.random() - 0.5) * 2 * (3 * Math.log(length) - 3)
  }
}
