import { Camera, DefaultRenderingPipeline, Scene, SSAO2RenderingPipeline, MotionBlurPostProcess, ScreenSpaceReflectionPostProcess } from '@babylonjs/core'
// 后处理
// 声明接口
type IPostProcessConfig = {
  msaaValue?:number,
  fxaaEnabled?:boolean,
  bloomEnabled?:boolean,
  bloomWeight?:number,
  glowLayerEnabled?:boolean
  glowLayer?:{blurKernelSize?:number, intensity?:number},
  depthOfFieldEnabled?:boolean,
  depthOfFieldBlurLevel?:number,
  depthOfField?:{
    fStop?:number,
    focalLength?:number,
    focusDistance?:number,
    lensSize?:number,
  }
  grainEnabled?:boolean,
  grain?:{
    animated:boolean,
    intensity:number
  },
  imageProcessingEnabled?:boolean,
  imageProcessing?:{
    contrast?:number,
    vignetteEnabled?:boolean
  },
  sharpenEnabled?:boolean,
  sharpen?:{
    edgeAmount?:number
  },
  ssaoEnabled?:boolean,
  SSAO?:{
    radius?:number,
    totalStrength?:number,
    expensiveBlur?:boolean,
    samples?:number,
    maxZ?:number
  },
  motionblurEnabled?:boolean,
  motionblur?:{
    motionStrength?:number,
    isObjectBased?:boolean
  },
  ssrEnabled?:boolean,
  ssr?:{
    reflectionSamples?:number,
    strength?:number,
    reflectionSpecularFalloffExponent?:number
  }
}

// 后处理类
class PostProcess {
  private scene!:Scene
  private camera!:Camera
  private defaultPipeline!:DefaultRenderingPipeline
  constructor (scene:Scene, camera:Camera, options?:IPostProcessConfig) {
    this.scene = scene
    this.camera = camera
    this.initDefaultPipeline(options)
  }

  private initDefaultPipeline (options?:IPostProcessConfig) {
    this.defaultPipeline = new DefaultRenderingPipeline('default', true, this.scene, [this.camera || this.scene.activeCamera])
    if (options) {
      // MSAA
      this.defaultPipeline.samples = options.msaaValue ?? 1
      // FXAA
      this.defaultPipeline.fxaaEnabled = options.fxaaEnabled ?? false
      // bloom光晕（会和运动模糊、ssao产生奇怪的冲突，建议使用glowLayer光晕）
      this.defaultPipeline.bloomEnabled = options.bloomEnabled ?? false
      this.defaultPipeline.bloomWeight = options.bloomWeight ?? 0.3
      // glowLayer光晕 兼容ssao2、运动模糊
      this.defaultPipeline.glowLayerEnabled = options.glowLayerEnabled ?? false
      if (this.defaultPipeline.glowLayer) {
        this.defaultPipeline.glowLayer.blurKernelSize = options.glowLayer?.blurKernelSize ?? 16
        this.defaultPipeline.glowLayer.intensity = options.glowLayer?.intensity ?? 0.3
      }
      // 景深
      this.defaultPipeline.depthOfFieldEnabled = options.depthOfFieldEnabled ?? false
      this.defaultPipeline.depthOfFieldBlurLevel = options.depthOfFieldBlurLevel ?? 0
      this.defaultPipeline.depthOfField.fStop = options.depthOfField?.fStop ?? 1.4
      this.defaultPipeline.depthOfField.focalLength = options.depthOfField?.focalLength ?? 50
      this.defaultPipeline.depthOfField.focusDistance = options.depthOfField?.focusDistance ?? 2000
      this.defaultPipeline.depthOfField.lensSize = options.depthOfField?.lensSize ?? 50 // 50 by default
      // 电影颗粒
      this.defaultPipeline.grainEnabled = options.grainEnabled ?? false
      this.defaultPipeline.grain.animated = options.grain?.animated ?? false
      this.defaultPipeline.grain.intensity = options.grain?.intensity ?? 5
      // 图片后处理
      this.defaultPipeline.imageProcessingEnabled = options.imageProcessingEnabled ?? false
      if (this.defaultPipeline.imageProcessing) {
        this.defaultPipeline.imageProcessing.contrast = options.imageProcessing?.contrast ?? 1.5
        this.defaultPipeline.imageProcessing.vignetteEnabled = options.imageProcessing?.vignetteEnabled ?? false
      }
      // 锐化边缘
      this.defaultPipeline.sharpenEnabled = options.sharpenEnabled ?? false
      this.defaultPipeline.sharpen.edgeAmount = options.sharpen?.edgeAmount ?? 0.5
      // SSAO
      if (options.ssaoEnabled) {
        const ssao = new SSAO2RenderingPipeline('ssao', this.scene, { ssaoRatio: 1.5, blurRatio: 0.5 })
        ssao.radius = options.SSAO?.radius || 3.5
        ssao.totalStrength = options.SSAO?.totalStrength || 0.5
        ssao.expensiveBlur = options.SSAO?.expensiveBlur || true
        ssao.samples = options.SSAO?.samples || 16
        ssao.maxZ = options.SSAO?.maxZ || 50
        this.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline('ssao', this.camera)
      }
      // 运动模糊
      if (options.motionblurEnabled) {
        const motionblur = new MotionBlurPostProcess('motionblur', this.scene, 1, this.camera)
        motionblur.motionStrength = options.motionblur?.motionStrength ?? 1
        motionblur.isObjectBased = options.motionblur?.isObjectBased ?? false
      }
      // ssr反射(对性能要求高，需要添加处理模型中的反射材质)
      if (options.ssrEnabled) {
        const ssr = new ScreenSpaceReflectionPostProcess('ssr', this.scene, 1.0, this.camera)
        ssr.reflectionSamples = options.ssr?.reflectionSamples ?? 32 // Low quality.
        ssr.strength = options.ssr?.strength ?? 1 // Set default strength of reflections.
        ssr.reflectionSpecularFalloffExponent = options.ssr?.reflectionSpecularFalloffExponent ?? 2
      }
    }
  }
}

export { PostProcess }
