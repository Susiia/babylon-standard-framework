// 粒子效果集

import { Color4, ParticleHelper, ParticleSystem, ParticleSystemSet, PointParticleEmitter, Scene, Texture, Vector3 } from '@babylonjs/core'

// 粒子
abstract class BasicParticle {
  protected scene:Scene
  protected ParticleSystemSet!:ParticleSystemSet|ParticleSystem
  constructor (position:Vector3, direction:Vector3, scene:Scene) {
    this.scene = scene
    this.makeParticles(position, direction)
  }

  protected abstract makeParticles (position:Vector3, direction:Vector3) :void

  // 销毁粒子系统
  public dispose () {
    this.ParticleSystemSet.dispose()
  }
}

// 烟雾粒子
class SmokeParticles extends BasicParticle {
  // 生成粒子系统
  protected makeParticles (position:Vector3, direction:Vector3) {
    ParticleHelper.CreateAsync('smoke', this.scene).then((set) => {
      this.ParticleSystemSet = set
      this.ParticleSystemSet.emitterNode = position
      this.ParticleSystemSet.systems[0].gravity = direction
      this.ParticleSystemSet.start()
    })
  }
}

// 火箭喷射粒子
class RockJetParticles extends BasicParticle {
  protected makeParticles (position:Vector3, direction:Vector3) {
    this.ParticleSystemSet = new ParticleSystem('particles', 100, this.scene)
    this.ParticleSystemSet.particleTexture = new Texture('./img/flare.png')
    this.ParticleSystemSet.emitter = position
    this.ParticleSystemSet.emitRate = 500
    const pointEmitter = new PointParticleEmitter()
    pointEmitter.direction1 = direction
    pointEmitter.direction2 = direction
    this.ParticleSystemSet.particleEmitterType = pointEmitter
    this.ParticleSystemSet.color1 = new Color4(0.5, 0.5, 1, 1)
    this.ParticleSystemSet.color2 = new Color4(0, 0, 1, 0.5)
    this.ParticleSystemSet.colorDead = new Color4(0, 0, 0, 0)
    this.ParticleSystemSet.maxSize = 0.5
    this.ParticleSystemSet.minSize = 0.01
    this.ParticleSystemSet.maxLifeTime = 1
    this.ParticleSystemSet.minLifeTime = 0.1
    this.ParticleSystemSet.updateSpeed = 0.1
    this.ParticleSystemSet.start()
  }
}

// TODO: 爆炸粒子

// TODO: 火焰粒子

// TODO: 小雨

// TODO: 大雨

// TODO: 暴风雨

// TODO: 小雪

// TODO: 大雪

// TODO: 暴风雪

// TODO:  雾气

// TODO: 星光

// TODO: 落叶

// TODO: 泡泡

export { SmokeParticles, RockJetParticles }
