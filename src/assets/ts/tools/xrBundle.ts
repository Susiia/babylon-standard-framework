/** babylon的vr组件
 * 公开方法：
 * constructor(grounds:AbstractMesh[], Scene:Scene)：构造方法，必传地面、场景
 * EnableTeleportation(interestingSpots?:Vector3[])：启用传送,可传Vector3数组，标识传送吸附热点位置
 * EnableMOVEMENT()：启用手柄移动
 * EnableHandTracking()：启用手识别
 * EnableWalkingLocomotion()：启用踏步移动
 * EnableControllerListener(writeToStore:boolean)：启用手柄监听
    xrBundleStateLT:左勾[.add(()=>{})][.clear()]
    xrBundleStateRT:右勾[.add(()=>{})][.clear()]
    xrBundleStateLS:左握[.add(()=>{})][.clear()]
    xrBundleStateRS:右握[.add(()=>{})][.clear()]
    xrBundleStateLTS:左摇杆[.add(()=>{})][.clear()]
    xrBundleStateRTS:摇杆[.add(()=>{})][.clear()]
    xrBundleStateX:X[.add(()=>{})][.clear()]
    xrBundleStateY:Y[.add(()=>{})][.clear()]
    xrBundleStateA:A[.add(()=>{})][.clear()]
    xrBundleStateB:B[.add(()=>{})][.clear()]
 */
import { Scene, AbstractMesh, WebXRDefaultExperience, Vector3, WebXRCamera, WebXRFeaturesManager, WebXRFeatureName, WebXRControllerComponent, WebXRInputSource, TransformNode, WebXRMotionControllerManager } from '@babylonjs/core'
import store from '@/store'
export default class XrBundle {
  private grounds:AbstractMesh[]
  private Scene:Scene
  private xrExperience!:WebXRDefaultExperience
  private featuresManager!:WebXRFeaturesManager
  /* 需要给我传个地面和场景 */
  constructor (grounds:AbstractMesh[], Scene:Scene, initDone:any) {
    this.grounds = grounds
    this.Scene = Scene
    WebXRMotionControllerManager.BaseRepositoryUrl = '/localize/xr-assets/'
    WebXRMotionControllerManager.PrioritizeOnlineRepository = false
    WebXRMotionControllerManager.UseOnlineRepository = false
    this.initWebxr().then(() => {
      // this.EnableTeleportation() // 传送
      this.EnableMOVEMENT() // 手柄行走
      // this.EnableWalkingLocomotion() // 踏步行走
      initDone() // vr处理完成之后执行的东西
    })
  }

  /* 初始化xr */
  public vrCamera!:WebXRCamera
  private async initWebxr ():Promise<void> {
    this.xrExperience = await this.Scene.createDefaultXRExperienceAsync()
    this.vrCamera = this.xrExperience.baseExperience.camera
    this.featuresManager = this.xrExperience.baseExperience.featuresManager
  }

  /* 启用传送 */
  public EnableTeleportation (interestingSpots?:Vector3[]):void {
    this.featuresManager.enableFeature(
      WebXRFeatureName.TELEPORTATION,
      'latest',
      {
        xrInput: this.xrExperience.input,
        floorMeshes: this.grounds,
        defaultTargetMeshOptions: {
          teleportationFillColor: '#e28832',
          teleportationBorderColor: '#e28832',
          disableLighting: true
        },
        snapPositions: interestingSpots, // 传送吸附热点
        snapToPositionRadius: 1.2
      }
    )
  }

  /* 启用手柄行走 */
  public EnableMOVEMENT ():void {
    // 手柄配置(左手行走，右手摇头)
    const swappedHandednessConfiguration = [
      {
        allowedComponentTypes: [WebXRControllerComponent.THUMBSTICK_TYPE, WebXRControllerComponent.TOUCHPAD_TYPE],
        forceHandedness: 'right',
        axisChangedHandler: (axes:any, movementState:any, featureContext:any, xrInput:any) => {
          movementState.rotateX = Math.abs(axes.x) > featureContext.rotationThreshold ? axes.x : 0
          movementState.rotateY = Math.abs(axes.y) > featureContext.rotationThreshold ? axes.y : 0
        }
      },
      {
        allowedComponentTypes: [WebXRControllerComponent.THUMBSTICK_TYPE, WebXRControllerComponent.TOUCHPAD_TYPE],
        forceHandedness: 'left',
        axisChangedHandler: (axes:any, movementState:any, featureContext:any, xrInput:any) => {
          movementState.moveX = Math.abs(axes.x) > featureContext.movementThreshold ? axes.x : 0
          movementState.moveY = Math.abs(axes.y) > featureContext.movementThreshold ? axes.y : 0
        }
      }
    ]
    // 相机重力
    this.xrExperience.input.xrCamera.checkCollisions = true
    this.xrExperience.input.xrCamera.applyGravity = true
    this.xrExperience.input.xrCamera.ellipsoid = new Vector3(0.15, 0.9, 0.15)
    // 禁用传送
    this.featuresManager.disableFeature(WebXRFeatureName.TELEPORTATION)
    // 启用手柄行走
    this.featuresManager.enableFeature(
      WebXRFeatureName.MOVEMENT, // 行走模式
      'latest' /* or latest */, // 最新版特性
      {
        xrInput: this.xrExperience.input, // xr输入
        customRegistrationConfigurations: swappedHandednessConfiguration, // 自定义手柄操作
        movementSpeed: 0.1, // 移动速度
        rotationSpeed: 1// 旋转速度
      })
  }

  /* 启用手识别 */
  public EnableHandTracking ():void {
    this.featuresManager.enableFeature(
      WebXRFeatureName.HAND_TRACKING,
      'latest',
      { xrInput: this.xrExperience.input }
    )
  }

  /* TODO:(碰撞检测不生效)启用踏步行走 */
  public EnableWalkingLocomotion ():void {
    // // 相机重力（不生效）
    // this.xrExperience.input.xrCamera.checkCollisions = true
    // this.xrExperience.input.xrCamera.applyGravity = true
    // this.xrExperience.input.xrCamera.ellipsoid = new Vector3(0.15, 0.9, 0.15)
    // 禁用传送
    this.featuresManager.disableFeature(WebXRFeatureName.TELEPORTATION)
    const xrRoot = new TransformNode('xrRoot', this.Scene)
    this.xrExperience.baseExperience.camera.parent = xrRoot
    // 启用踏步行走
    this.featuresManager.enableFeature(
      WebXRFeatureName.WALKING_LOCOMOTION,
      'latest', // 最新版特性
      { locomotionTarget: xrRoot }
    )
  }

  /* 手柄组件暴露  */
  public xrBundleStateLT!: WebXRControllerComponent // 左勾
  public xrBundleStateRT!: WebXRControllerComponent // 右勾
  public xrBundleStateLS!: WebXRControllerComponent // 左握
  public xrBundleStateRS!: WebXRControllerComponent // 右握
  public xrBundleStateLTS!: WebXRControllerComponent // 左摇杆
  public xrBundleStateRTS!: WebXRControllerComponent // 右摇杆
  public xrBundleStateX!: WebXRControllerComponent // X
  public xrBundleStateY!: WebXRControllerComponent // Y
  public xrBundleStateA!: WebXRControllerComponent // A
  public xrBundleStateB!: WebXRControllerComponent // B

  /* NOTE:手柄事件方法  */
  public EnableControllerListener (writeToStore:boolean):void {
    this.xrExperience.input.onControllerAddedObservable.clear()
    this.xrExperience.input.onControllerAddedObservable.add((eventData: WebXRInputSource) => {
      eventData.onMotionControllerInitObservable.add((motionController) => {
        // 左手
        if (motionController.handness === 'left') {
          const xrIds = motionController.getComponentIds()
          // 勾
          const triggerComponent = motionController.getComponent(xrIds[0])
          // 握
          const squeezeComponent = motionController.getComponent(xrIds[1])// xr-standard-squeeze
          // 摇杆
          const thumbstickComponent = motionController.getComponent(xrIds[2])// xr-standard-thumbstick
          // Y
          const ybuttonComponent = motionController.getComponent(xrIds[4])// y-button
          // X
          const xbuttonComponent = motionController.getComponent(xrIds[3])// x-button
          if (writeToStore) {
            triggerComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_LTt',
                value: triggerComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_LT',
                value: triggerComponent.pressed
              })
              store.commit('setState', {
                name: 'xrBundleState_LTv',
                value: triggerComponent.value
              })
            })
            squeezeComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_LSt',
                value: squeezeComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_LS',
                value: squeezeComponent.pressed
              })
              store.commit('setState', {
                name: 'xrBundleState_LSv',
                value: squeezeComponent.value
              })
            })
            thumbstickComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_LTSt',
                value: thumbstickComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_LTSp',
                value: thumbstickComponent.pressed
              })
            })
            thumbstickComponent.onAxisValueChangedObservable.add((axes) => {
              store.commit('setState', {
                name: 'xrBundleState_LTSx',
                value: axes.x
              })
              store.commit('setState', {
                name: 'xrBundleState_LTSx',
                value: axes.y
              })
            })
            xbuttonComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_Xt',
                value: xbuttonComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_X',
                value: xbuttonComponent.pressed
              })
            })
            ybuttonComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_Yt',
                value: ybuttonComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_Y',
                value: ybuttonComponent.pressed
              })
            })
          }
          this.xrBundleStateLT = triggerComponent
          this.xrBundleStateLS = squeezeComponent
          this.xrBundleStateLTS = thumbstickComponent
          this.xrBundleStateY = ybuttonComponent
          this.xrBundleStateX = xbuttonComponent
        }
        // 右手
        if (motionController.handness === 'right') {
          const xrIds = motionController.getComponentIds()
          // 勾
          const triggerComponent = motionController.getComponent(xrIds[0])// xr-standard-trigger
          // 握
          const squeezeComponent = motionController.getComponent(xrIds[1])// xr-standard-squeeze
          // 摇杆按下
          const thumbstickComponent = motionController.getComponent(xrIds[2])// xr-standard-thumbstick
          // A
          const abuttonComponent = motionController.getComponent(xrIds[3])// a-button
          // B
          const bbuttonComponent = motionController.getComponent(xrIds[4])// b-button
          if (writeToStore) {
            triggerComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_RTt',
                value: triggerComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_RT',
                value: triggerComponent.pressed
              })
              store.commit('setState', {
                name: 'xrBundleState_RTv',
                value: triggerComponent.value
              })
            })
            squeezeComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_RSt',
                value: squeezeComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_RS',
                value: squeezeComponent.pressed
              })
              store.commit('setState', {
                name: 'xrBundleState_RSv',
                value: squeezeComponent.value
              })
            })
            thumbstickComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_RTSt',
                value: thumbstickComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_RTSp',
                value: thumbstickComponent.pressed
              })
            })
            thumbstickComponent.onAxisValueChangedObservable.add((axes) => {
              store.commit('setState', {
                name: 'xrBundleState_RTSx',
                value: axes.x
              })
              store.commit('setState', {
                name: 'xrBundleState_RTSx',
                value: axes.y
              })
            })
            abuttonComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_At',
                value: abuttonComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_A',
                value: abuttonComponent.pressed
              })
            })
            bbuttonComponent.onButtonStateChangedObservable.add(() => {
              store.commit('setState', {
                name: 'xrBundleState_Bt',
                value: bbuttonComponent.touched
              })
              store.commit('setState', {
                name: 'xrBundleState_B',
                value: bbuttonComponent.pressed
              })
            })
          }
          this.xrBundleStateRT = triggerComponent
          this.xrBundleStateRS = squeezeComponent
          this.xrBundleStateRTS = thumbstickComponent
          this.xrBundleStateA = abuttonComponent
          this.xrBundleStateB = bbuttonComponent
        }
      })
    })
  }
}
