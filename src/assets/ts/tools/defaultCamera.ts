/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-04 09:10:39
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-11 11:24:33
 */
import { Scene, UniversalCamera, Vector3, Animation, CircleEase, EasingFunction } from '@babylonjs/core'

export default function
defaultCamera (scene:Scene, canvas:HTMLCanvasElement, jump?:boolean) {
  const camera = new UniversalCamera('defaultCamera', new Vector3(0, 0, 0), scene)
  camera.ellipsoid = new Vector3(0.15, 0.9, 0.15)
  camera.speed = 0.2
  // this.camera.checkCollisions = true
  // this.camera.applyGravity = true
  camera.attachControl(canvas, true)
  camera.minZ = 0
  camera.fov = 0.4
  if (jump) { cameraJump(scene) }
  return camera
}

function cameraJump (scene:Scene):void {
  window.addEventListener('keyup', function (e) {
    switch (e.key) {
      case ' ':
        jump()
        break
    }
  }, false)
  const jump = () => {
    const cam = scene.cameras[0]
    cam.animations = []
    const a = new Animation('a', 'position.y', 20, Animation.ANIMATIONTYPE_FLOAT, Animation.ANIMATIONLOOPMODE_CYCLE)
    const keys = []
    keys.push({ frame: 0, value: cam.position.y })
    keys.push({ frame: 5, value: cam.position.y + 1 })
    keys.push({ frame: 15, value: cam.position.y })
    a.setKeys(keys)
    const easingFunction = new CircleEase()
    easingFunction.setEasingMode(EasingFunction.EASINGMODE_EASEINOUT)
    a.setEasingFunction(easingFunction)
    cam.animations.push(a)
    scene.beginAnimation(cam, 0, 20, false)
  }
}
