/*
 * @Descripttion: 简单后处理
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-18 10:09:50
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-18 16:05:55
 */
import { Camera, DefaultRenderingPipeline, Scene, SSAO2RenderingPipeline, SSAORenderingPipeline } from '@babylonjs/core'

export default class SimplePostProcess {
    private scene!:Scene
    private camera!:Camera
    private defaultPipeline!:DefaultRenderingPipeline
    /**
     * @Descripttion:
     * @Author: 刘译蓬
     * @msg:
     * @param {Scene} scene
     * @param {Camera} camera
     * @return {*}
     */
    constructor (scene:Scene) {
      console.log(scene)
      // console.log(camera)
      this.scene = scene
      this.camera = this.scene.activeCamera!
      this.initDefaultPipeline()
      this.scene.getEngine().resize()
    }

    private initDefaultPipeline () {
      this.defaultPipeline = new DefaultRenderingPipeline('default', true, this.scene, [this.camera])
      // 发光层
      this.defaultPipeline.glowLayerEnabled = true
      if (this.defaultPipeline.glowLayer) this.defaultPipeline.glowLayer.blurKernelSize = 16
      if (this.defaultPipeline.glowLayer) this.defaultPipeline.glowLayer.intensity = 0.3
      // SSAO
      const ssao = new SSAORenderingPipeline('ssao', this.scene, 0.5)
      ssao.fallOff = 0.000001
      ssao.area = 1
      ssao.radius = 0.0001
      ssao.totalStrength = 0.5
      ssao.base = 0.5
      this.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline(
        'ssao',
        this.camera
      )
      // SSAO2
      // const ssao2 = new SSAO2RenderingPipeline('ssao', this.scene, {
      //   ssaoRatio: 1.5,
      //   blurRatio: 0.5
      // })
      // ssao2.radius = 3.5
      // ssao2.totalStrength = 0.5
      // ssao2.expensiveBlur = true
      // ssao2.samples = 16
      // ssao2.maxZ = 50
      // this.scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline(
      //   'ssao',
      //   this.camera
      // )
    }
}
