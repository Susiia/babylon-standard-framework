/*
 * @Author: 刘译蓬 liuyipeng520@qq.com
 * @Date: 2022-05-25 10:24:13
 * @LastEditors: 刘译蓬 liuyipeng520@qq.com
 * @LastEditTime: 2022-05-25 12:56:18
 * @FilePath: \standard-framework\src\assets\ts\webRTCgetCamera.js
 * @Description: 这是默认设置,请设置`customMade`, 打开koroFileHeader查看配置 进行设置: https://github.com/OBKoro1/koro1FileHeader/wiki/%E9%85%8D%E7%BD%AE
 */
// renderCameraToCanvas
// Cvideo是一个video标签
export default class CameraToVideo {
  constructor (Cvideo) {
    console.log(Cvideo)
    const constraints = {
      audio: false,
      video: {
        width: 480, // 宽
        height: 320, // 高
        frameRate: 30, // 帧率
        facingMode: 'environment'// 后置摄像头
      }
    }
    function successCallback (stream) {
      console.log(stream)
      console.log('this.Cvideo')
      console.log(Cvideo)
      Cvideo.srcObject = stream
      Cvideo.onloadedmetadata = function (e) {
        Cvideo.play()
      }
    }
    if (navigator.mediaDevices.getUserMedia) {
      navigator.mediaDevices.getUserMedia(constraints).then(successCallback)
    }

    this.renderCameraToCanvas()
  }

  // renderCameraToCanvas () {

  // }
}
