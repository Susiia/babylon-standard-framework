import { Scene, UniversalCamera, Vector3 } from '@babylonjs/core'

// 陀螺仪相机
export default class GyroscopeCamera {
  public name:string
  public position:Vector3
  public scene:Scene|undefined
  public camera:UniversalCamera
  constructor (name: string, position: Vector3, scene?: Scene) {
    this.name = name
    this.position = position
    this.scene = scene
    this.camera = new UniversalCamera(this.name, this.position, this.scene)
    // 获取陀螺仪参数
    this.getGyroscope()
  }

  // 获取手机状态
  private getGyroscope () {
    // 获取速度
    // window.addEventListener('devicemotion', function (event) {
    // 获取当前旋转角度
    window.addEventListener('deviceorientation', (event: DeviceOrientationEvent) => {
      this.gyroscope2Vector3(event)
    })
  }

  // gyroscope2Vector3(传入陀螺仪deviceorientation事件)
  private gVector3 = new Vector3(0, 0, 0)
  private gyroscope2Vector3 (event: DeviceOrientationEvent): void {
    if (Math.abs(window.orientation) === 90) { // 屏幕为横
      // 左右
      if (event.gamma! > 0) {
        this.camera.rotation.y = this.angle2radian(0 - event.alpha!)
      } else {
        this.camera.rotation.y = this.angle2radian(180 - event.alpha!)
      }
      // 上下
      this.camera.rotation.x = this.angle2radian(event.gamma! > 0 ? event.gamma! - 90 : event.gamma! + 90)
      // console.log(`a${event.alpha}`)
      // console.log(`b${event.beta}`)
      // console.log(`g${event.gamma}`)
    }
  }

  // 角度转弧度
  private angle2radian (angle:number|null) {
    return angle ? angle * (Math.PI / 180) : 0
  }
  /*
  set&get
  */

  // ellipsoid
  public get ellipsoid (): Vector3 {
    return this.camera.ellipsoid
  }

  public set ellipsoid (value: Vector3) {
    this.camera.ellipsoid = value
  }

  // speed
  public get speed (): number {
    return this.camera.speed
  }

  public set speed (value: number) {
    this.camera.speed = value
  }

  // checkCollisions
  public get checkCollisions (): boolean {
    return this.camera.checkCollisions
  }

  public set checkCollisions (value: boolean) {
    this.camera.checkCollisions = value
  }

  // checkCollisions
  public get applyGravity (): boolean {
    return this.camera.applyGravity
  }

  public set applyGravity (value: boolean) {
    this.camera.applyGravity = value
  }

  // attachControl
  public attachControl (ignored: any, noPreventDefault?: boolean): void {
    this.camera.attachControl(ignored, noPreventDefault)
  }

  // minZ
  public get minZ (): number {
    return this.camera.minZ
  }

  public set minZ (value: number) {
    this.camera.minZ = value
  }

  // maxZ
  public get maxZ (): number {
    return this.camera.maxZ
  }

  public set maxZ (value: number) {
    this.camera.maxZ = value
  }
}
