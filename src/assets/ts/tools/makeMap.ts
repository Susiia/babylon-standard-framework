import { AbstractMesh, Scene, SceneLoader, TransformNode } from '@babylonjs/core'
import '@babylonjs/loaders'
import '@babylonjs/loaders/glTF'
// 地图生成
export default class GameMap {
  private scene!:Scene // Scene
  private groundNode!:TransformNode
  constructor (scene: Scene) {
    this.scene = scene
    // 生成节点
    this.makeNodes()
    // 加载模型
    this.loadModel('/model/GLTF format/', 'ground_pathOpen.glb')
    // this.loadModel('/model/', 'scene1chaifen.glb')
  }

  // 生成节点
  private makeNodes () {
    // 地面节点
    this.groundNode = new TransformNode('groundNode', this.scene)
  }

  // 加载模型
  public loadModel (path:string, fileName?:string|undefined):void {
    SceneLoader.LoadAssetContainer(
      path,
      fileName,
      this.scene,
      (container) => {
        this.modelTreatment(container.meshes)
        // container.addAllToScene()
      }
    )
  }

  // 模型处理
  private modelTreatment (meshes:AbstractMesh[]):void {
    meshes.forEach(r => {
      if (r.name.indexOf('ground') >= 0) {
        r.checkCollisions = true
        this.groundEach(r)
      }
    })
  }

  // 地面循环
  private groundEach (groundMesh:AbstractMesh) {
    for (let i = 0; i < 100; i++) {
      for (let j = 0; j < 100; j++) {
        const groundTemp = groundMesh.clone('groundMesh', this.groundNode)
        groundTemp!.position.x = i + 1
        groundTemp!.position.z = j
        groundTemp!.checkCollisions = true
      }
    }
  }
}
