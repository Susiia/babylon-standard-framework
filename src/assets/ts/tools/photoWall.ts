/*
 * @Descripttion:本地文件用的
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-05-07 15:58:32
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-20 08:43:09
 */
import { ArcRotateCamera, ArcRotateCameraPointersInput, Color3, Mesh, MeshBuilder, PBRMaterial, Scene, Texture, UniversalCamera, Vector3, ISize, VideoTexture, Engine, Color4, DefaultRenderingPipeline } from '@babylonjs/core'
import '@babylonjs/inspector'
import TWEEN from '@tweenjs/tween.js'
// 照片墙
export default class PhotoWall {
  private engine!:Engine // Engine
  private canvas!:HTMLCanvasElement // canvas
  private camera!:ArcRotateCamera
  private photoJson!:{ name: string; path: string; }[]
  private cameraTarget!:Vector3
  private scene!:Scene // Scene
  private videoList!:Map<string, HTMLVideoElement>
  // constructor (canvas:HTMLCanvasElement, fileList:FileList) {
  constructor (canvas:HTMLCanvasElement, fileList:[{ name: string; path: string; }]) {
    if (canvas) {
      this.canvas = canvas
      this.initEngine(canvas)
      this.debuggerLayer()
      this.photoJson = fileList
      // this.initPhotoJson(fileList)
      this.initCamera()
      this.initPostProcess()
      this.makePhotoCloud(this.photoJson)
    }
  }

  // 初始化照片数据
  private initPhotoJson (fileList:FileList) {
    this.videoList = new Map()
    this.photoJson = []
    for (let index = 0; index < fileList.length; index++) {
      const file = fileList[index]
      this.photoJson.push({ name: file.name, path: URL.createObjectURL(file) })
    }
  }

  // 初始化引擎
  private async initEngine (canvas: HTMLCanvasElement): Promise<void> {
    this.canvas = canvas // 初始化canvas
    this.engine = new Engine(
      this.canvas,
      true,
      {},
      true
    )
    this.scene = new Scene(this.engine)// 初始化场景
    this.scene.clearColor = new Color4(0.784, 0.878, 1, 1)
    const assumedFramesPerSecond = 60
    const earthGravity = -9.34
    this.scene.gravity = new Vector3(0, earthGravity / assumedFramesPerSecond, 0) // 设置场景重力(模拟地球)
    this.scene.collisionsEnabled = true // 开启碰撞检测
    this.RenderLoop()// 执行渲染循环
  }

  // 渲染循环
  private RenderLoop ():void {
    // 添加窗口变化事件监听
    window.addEventListener('resize', () => {
      this.engine.resize()
    })
    // 执行循环
    this.engine.runRenderLoop(() => {
      this.scene.render()
      TWEEN.update()
    })
  }

  // debuggerLayer（Shift+Ctrl+Alt+I）
  private debuggerLayer ():void {
    window.addEventListener('keydown', (ev) => {
      if (ev.shiftKey && ev.ctrlKey && ev.altKey && ev.keyCode === 73) {
        if (this.scene.debugLayer.isVisible()) {
          this.scene.debugLayer.hide()
        } else {
          this.scene.debugLayer.show(
            { embedMode: true }
          )
        }
      }
    })
  }

  // 初始化后处理
  private initPostProcess (camera?:UniversalCamera|ArcRotateCamera, scene?:Scene):void {
    const defaultPipeline = new DefaultRenderingPipeline('default', true, scene || this.scene, [camera || this.camera])

    // glowLayer光晕 兼容ssao2、运动模糊
    defaultPipeline.glowLayerEnabled = true
    if (defaultPipeline.glowLayer) {
      defaultPipeline.glowLayer.blurKernelSize = 16 // 16 by default
      defaultPipeline.glowLayer.intensity = 0.3 // 1 by default
    }

    // 景深
    defaultPipeline.depthOfFieldEnabled = true
    defaultPipeline.depthOfFieldBlurLevel = 0 // 0 by default
    defaultPipeline.depthOfField.fStop = 1.4 // 1.4 by default
    defaultPipeline.depthOfField.focalLength = 50 // 50 by default, mm
    defaultPipeline.depthOfField.focusDistance = 2000 // 2000 by default, mm
    defaultPipeline.depthOfField.lensSize = 50 // 50 by default

    // 电影颗粒
    defaultPipeline.grainEnabled = true
    defaultPipeline.grain.animated = true
    defaultPipeline.grain.intensity = 5

    // 图片后处理
    defaultPipeline.imageProcessing.contrast = 1.5
    defaultPipeline.imageProcessing.vignetteEnabled = true

    // 清晰边缘
    defaultPipeline.sharpenEnabled = true
    defaultPipeline.sharpen.edgeAmount = 0.5
  }

  // 初始化旋转相机
  initCamera (): void {
    this.cameraTarget = new Vector3(0, 0, 0)
    this.camera = new ArcRotateCamera('ArcRotateCamera', 0, Math.PI / 2, 3 * Math.log(Object.keys(this.photoJson).length), this.cameraTarget)
    this.camera.minZ = 1.4
    this.camera.fovMode = ArcRotateCamera.FOVMODE_VERTICAL_FIXED
    // 相机控制
    this.camera.inputs.clear()
    const inputs = new ArcRotateCameraPointersInput()
    inputs.angularSensibilityX = 5000
    inputs.angularSensibilityY = 5000
    inputs.panningSensibility = 5000
    inputs.pinchDeltaPercentage = 0.0001
    inputs.pinchInwards = false
    this.camera.inputs.add(inputs)
    this.camera.inputs.addMouseWheel()
    this.camera.attachControl(this.canvas, true)
    // 相机行为
    this.camera.inertia = 0.98
    this.camera.panningInertia = 0.98
    this.camera.lowerRadiusLimit = 1
    this.camera.upperRadiusLimit = 3 * Math.log(Object.keys(this.photoJson).length) + 20
    this.camera.wheelDeltaPercentage = 0.0001
    this.camera.zoomToMouseLocation = true
    this.camera.useAutoRotationBehavior = true
    this.camera.autoRotationBehavior!.idleRotationWaitTime = 0
    this.camera.autoRotationBehavior!.idleRotationSpinupTime = 10
    this.camera.speed = 0
    //
    let currentFile = ''
    this.scene.onPointerObservable.add(e => {
      // 点空白
      if (e.type === 32) {
        if (!e.pickInfo!.pickedMesh) {
          this.fadeAnimation(this.camera.target, { x: 0, y: 0, z: 0 })
          const radiusTemp = {
            _radius: this.camera.radius,
            _camera: this.camera,
            get radius () {
              return this._radius
            },
            set radius (value) {
              this._camera.radius = value
              this._radius = value
            }
          }
          this.fadeAnimation(radiusTemp, { radius: 3 * Math.log(Object.keys(this.photoJson).length) })
          this.videoList.forEach(r => {
            r.pause()
            r.currentTime = r.duration / 2
            r.muted = true
          })
        }
      }
      // 点到mesh
      if (e.type === 16) {
        this.fadeAnimation(this.camera.target, e.pickInfo!.pickedMesh!.position)
        const radiusTemp = {
          _radius: this.camera.radius,
          _camera: this.camera,
          get radius () {
            return this._radius
          },
          set radius (value) {
            this._camera.radius = value
            this._radius = value
          }
        }
        this.fadeAnimation(radiusTemp, { radius: 2.5 })
        // 如果是视频的话就解除静音
        if (e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name && e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name.indexOf('videoTexture') !== -1 && e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name !== currentFile) {
          if (currentFile.indexOf('videoTexture') !== -1) {
            this.videoList.get(currentFile)?.pause()
            this.videoList.get(currentFile)!.currentTime = this.videoList.get(currentFile)!.duration / 2
            this.videoList.get(currentFile)!.muted = true
          }
          currentFile = e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name
          const video = this.videoList.get(e.pickInfo!.pickedMesh!.material?.getActiveTextures()[0].name)!
          video.muted = false
          video.currentTime = 0
          video.play()
        }
      }
    })
  }

  // 随机生成照片散点
  makePhotoCloud (photoList:{name:string, path:string}[]):void {
    photoList.forEach((r, i) => {
      if (r.name.indexOf('.mp4') !== -1) {
        // video
        // texture
        const videoTextureTemp = new VideoTexture('videoTexture' + i, r.path, this.scene, false, false, VideoTexture.TRILINEAR_SAMPLINGMODE,
          {
            autoPlay: false,
            muted: true,
            autoUpdateTexture: true
          }
        )
        this.videoList.set(videoTextureTemp.name, videoTextureTemp.video)
        videoTextureTemp.onLoadObservable.add(texture => {
          const textureSize = texture.getSize()
          // material
          const videoMaterialTemp = new PBRMaterial('videoMaterial' + i, this.scene)
          // videoMaterialTemp.albedoTexture = videoTextureTemp
          videoMaterialTemp.emissiveTexture = videoTextureTemp
          videoMaterialTemp.emissiveColor = new Color3(1, 1, 1)
          videoMaterialTemp.metallic = 0
          videoMaterialTemp.roughness = 0
          // mesh
          const videoPlanTemp = MeshBuilder.CreatePlane('photoPlan' + i, this.meshSize(textureSize))
          videoPlanTemp.material = videoMaterialTemp
          videoPlanTemp.billboardMode = Mesh.BILLBOARDMODE_ALL
          // position
          videoPlanTemp.position = new Vector3(
            (Math.random() - 0.5) * 2 * (3 * Math.log(Object.keys(photoList).length)),
            (Math.random() - 0.5) * 2 * (3 * Math.log(Object.keys(photoList).length)),
            (Math.random() - 0.5) * 2 * (3 * Math.log(Object.keys(photoList).length))
          )
        })
      } else if (r.name.indexOf('.png') !== -1 || r.name.indexOf('.jpg') !== -1) {
        // img
        // texture
        const photoTextureTemp = new Texture(r.path, this.scene)
        photoTextureTemp.onLoadObservable.add(texture => {
          const textureSize = texture.getSize()
          // material
          const photoMaterialTemp = new PBRMaterial('photoMaterial' + i, this.scene)
          photoMaterialTemp.albedoTexture = photoTextureTemp
          photoMaterialTemp.emissiveTexture = photoMaterialTemp.albedoTexture
          photoMaterialTemp.emissiveColor = new Color3(1, 1, 1)
          photoMaterialTemp.metallic = 0
          photoMaterialTemp.roughness = 0
          // mesh
          const photoPlanTemp = MeshBuilder.CreatePlane('photoPlan' + i, this.meshSize(textureSize))
          photoPlanTemp.material = photoMaterialTemp
          photoPlanTemp.billboardMode = Mesh.BILLBOARDMODE_ALL
          // position
          photoPlanTemp.position = new Vector3(
            (Math.random() - 0.5) * 2 * (3 * Math.log(Object.keys(photoList).length)),
            (Math.random() - 0.5) * 2 * (3 * Math.log(Object.keys(photoList).length)),
            (Math.random() - 0.5) * 2 * (3 * Math.log(Object.keys(photoList).length))
          )
        })
      }
    })
  }

  // 过渡动画方法
  private fadeAnimation (target: any, to: any) {
    const tw = new TWEEN.Tween(target).to(to, 1000)
    tw.easing(TWEEN.Easing.Cubic.InOut)//
    tw.start()
  }

  // meshSize
  private meshSize (textureSize: ISize) {
    return textureSize.height > textureSize.width ? { height: 2, width: (textureSize.width / textureSize.height) * 2 } : { height: (textureSize.height / textureSize.width) * 2, width: 2 }
  }
}
