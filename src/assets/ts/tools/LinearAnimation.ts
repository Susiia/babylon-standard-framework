/*
 * @Descripttion: 线性动画
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-01 09:16:22
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-19 16:11:15
 */
import { AnimationGroup, Engine, DeviceSourceManager, Scene, IWheelEvent } from '@babylonjs/core'
export default class LinearAnimation {
    private animationGroups:AnimationGroup[]
    private scene:Scene // 场景
    private engine:Engine // 引擎
    private wheelFlag={ type: 0, deltaTime: 0 } // 滚轮下标：type 0 为反向，1为正向
    private playing = false // 播放
    private reverse = false // 反向
    private framesDifference:number // 滚轮播放判定帧差
    private playRate:number // 播放速率
    private isStarted = false // 开始下标
    private isEnded = false // 结束下标
    private onEnded:(() => void) | undefined
    /**
     * @Descripttion:线性动画
     * @Author: 刘译蓬
     * @msg:
     * @param {Scene} scene
     * @param {Engine} engine
     * @param {AnimationGroup} animationGroups
     * @param {number} framesDifference
     * @param {number} playRate
     * @param {function} onEnded
     * @return {*}
     */
    constructor (scene:Scene, engine:Engine, animationGroups:AnimationGroup[], framesDifference:number, playRate:number, onEnded?:()=>void) {
      this.animationGroups = animationGroups // 动画组列表
      this.scene = scene // 场景
      this.engine = engine // 引擎
      this.framesDifference = framesDifference // 滚轮播放判定帧差
      this.playRate = playRate // 播放速率
      this.onEnded = onEnded // 动画结束事件
      this.initanimationEvent() // 动画事件
      this.upDatePlayState(false, false) // 停止播放所有动画
      this.mouseWhell() // 鼠标滚轮播放状态控制器
    }

    // 动画组事件
    private initanimationEvent () {
      // 将所有动画组的loop模式改为false
      this.animationGroups.forEach(item => {
        item.loopAnimation = false
      })
      // 添加动画组结束事件监听
      this.animationGroups[0].onAnimationGroupEndObservable.add((ed, es) => {
        this.isEnded = true // 结束后结束下标为真
        this.animationGroups.forEach(item => { item.pause() })
        if (this.onEnded) { this.onEnded() }
      })
      // 循环事件监听，一旦循环就触发帧跳转
      this.animationGroups[0].onAnimationGroupLoopObservable.add((ed, es) => {
        this.animationGroups.forEach(item => { item.goToFrame(10) })
      })
    }

    // 滚轮状态判断
    private mouseWhell () {
      let deltaTime = this.framesDifference + 1 // 滚轮判定帧起点+1
      // 初始化设备管理器
      const dsm = new DeviceSourceManager(this.engine)
      // 设备连接管理器
      dsm.onDeviceConnectedObservable.add((device) => {
        // 如果设备号为2则代表鼠标已连接
        if (device.deviceType === 2) {
          // 添加输入事件监听
          device.onInputChangedObservable.add((e) => {
            let event
            // e的inputIndex为8则代表是滚轮事件
            if (e.inputIndex === 8) {
              event = e as IWheelEvent
              if (event.wheelDelta && event.wheelDelta > 0) {
                // 滚轮向上
                this.wheelFlag.type = 0
                this.wheelFlag.deltaTime = deltaTime
              } else {
                // 滚轮向下
                this.wheelFlag.type = 1
                this.wheelFlag.deltaTime = deltaTime
              }
            }
          })
        }
      })
      // 渲染循环
      this.scene.registerAfterRender(() => {
        // 如果结束下标为真则跳过滚轮事件处理
        if (this.isEnded) { return }
        // 如果上一次滚轮时间和下一次滚轮事件相差不超过this.framesDifference
        if (deltaTime - this.wheelFlag.deltaTime > this.framesDifference) {
          // 停止
          if (this.playing) {
            this.upDatePlayState(false, false)
          }
          this.playing = false
        } else {
          // 播放
          if (!this.playing || this.reverse !== (this.wheelFlag.type !== 0)) {
            this.upDatePlayState(true, this.wheelFlag.type !== 0)
          }
          this.playing = true
          this.reverse = this.wheelFlag.type !== 0
        }
        deltaTime++
      })
    }

    // 动画播放控制器
    private upDatePlayState (playing:boolean, reverse:boolean) {
      this.isStarted = this.isStarted || reverse
      if (playing) {
        this.animationGroups.forEach((item, index) => {
          item.speedRatio = reverse ? -this.playRate : this.playRate
          //   console.log(reverse ? '反播放' : '正播放')
          item.play()
        })
      } else {
        this.animationGroups.forEach((item, index) => {
        //   console.log('暂停')
          item.pause()
        })
      }
    }
}
