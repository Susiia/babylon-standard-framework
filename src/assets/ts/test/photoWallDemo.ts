/*
 * @Descripttion:
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-07-20 09:04:11
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-20 09:42:47
 */
import PhotoWall from '../tools/photoWall2'
const photoList = {
  photo1: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false },
  photo2: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false },
  photo3: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: true },
  photo4: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false },
  photo5: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false },
  photo6: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: true },
  photo7: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false },
  photo8: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: true },
  photo9: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false },
  photo10: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: true },
  photo11: { url: 'https://lyp-static.oss-cn-beijing.aliyuncs.com/img/202207192110849.png', content: 'photo1', isClick: false }
}

export default function photoWallDemo (canvas: HTMLCanvasElement) {
  const photoWallInstance = new PhotoWall(canvas, photoList, 'https://lyp-static.oss-cn-beijing.aliyuncs.com/video/photoWallDemo/effect1.mp4')
  photoWallInstance.interactionOnClick(id => {
    console.log('您点击了：' + id)
  })
}
