import { Scene, Engine, SceneLoader, AssetContainer, Vector3, UniversalCamera, Material } from '@babylonjs/core'
import LinearAnimation from '../tools/LinearAnimation'
import { SmokeParticles } from '../tools/smokeParticles'
/**
 * @Descripttion: 线性动画测试
 * @Author: 刘译蓬
 * @msg:
 * @param {Scene} scene
 * @param {Engine} engine
 * @param {UniversalCamera} camera
 * @return {*}
 */
const linearAnimationTest = (scene:Scene, engine:Engine, camera:UniversalCamera) => {
  // 加载模型
  SceneLoader.LoadAssetContainerAsync(
    '/model/', // 路径
    'animationTest2.glb', // 文件名
    scene, // 场景
    (progress) => { // 加载进程回调
      console.log('loading:' + progress.loaded) // 加载进度
    }
  ).then(
    (container) => {
      modelTreatment(container, scene, camera) // 模型处理
      container.addAllToScene() // 加入场景
      // 线性动画
      const temp = new LinearAnimation(scene, engine, container.animationGroups, 25, 3, () => {
        console.log('ended')
      })
    }
  ).finally(() => {
    console.log('loaded') // 模型加载结束事件
  })
}

/**
   * @Descripttion: 模型处理
   * @Author: 刘译蓬
   * @msg:模型处理
   * @param {AssetContainer} container
   * @return {*}
   */
const modelTreatment = (container:AssetContainer, scene:Scene, camera:UniversalCamera) => {
  // meshs
  container.meshes.forEach(meshe => {
    // meshe.checkCollisions = true
    // meshe.receiveShadows = true
  })
  // nodes
  container.transformNodes.forEach(node => {
    // 烟雾处理
    if (node.name.indexOf('smokeNode') !== -1) {
      const somke = new SmokeParticles(node.position, new Vector3(0, 1, 0), scene)
    }
    // 相机位置处理
    if (node.name.indexOf('Camera') !== -1) {
      //! 坐标系问题，需要设置一下当作父级的node的scaling以及rotation
      node.scaling.z = -1
      camera.parent = node
    }
  })
  // Material
  container.materials.forEach((material) => {
    material.transparencyMode = Material.MATERIAL_ALPHATEST // 透明类型都改成钳制（alphaTest）
    console.log(material.transparencyMode)
  })
}

export default linearAnimationTest
