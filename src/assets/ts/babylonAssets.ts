/*
 * @Descripttion: babylon入口文件
 * @version:
 * @Author: 刘译蓬
 * @Date: 2022-06-24 15:41:02
 * @LastEditors: 刘译蓬
 * @LastEditTime: 2022-07-22 16:55:05
 */
/* 基本依赖导入 */
import { Engine, Scene, UniversalCamera, DeviceOrientationCamera, PhotoDome } from '@babylonjs/core'
import '@babylonjs/loaders'
import '@babylonjs/loaders/glTF'
import '@babylonjs/inspector'
/* 核心模块导入 */
import initEngine from '@/assets/ts/utils/initEngine'
import debuggerLayer from './utils/debuggerLayer'
/* 组件导入 */
import skyLight from './tools/skyLight' // 天光
import defaultCamera from './tools/defaultCamera' // 默认相机
/* 测试 */
import modelLoadTest from './test/modelLoadTest' // 测试加载
import linearAnimationTest from './test/linearAnimationTest' // 测试线性动画
import photoWallDemo from './test/photoWallDemo' // 测试照片墙
export default class BabylonAssets {
  private canvas!:HTMLCanvasElement // canvas
  private engine!:Engine // Engine
  private scene!:Scene // Scene
  private camera!:UniversalCamera|DeviceOrientationCamera // Camera
  /**
   * @Descripttion: 需要传入的数据
   * @Author: 刘译蓬
   * @msg:
   * @param {HTMLCanvasElement} canvas
   * @return {*}
   */
  constructor (canvas:HTMLCanvasElement|null) {
    if (canvas) {
      // 赋值canvas
      this.canvas = canvas
      // 初始化引擎
      const { engine, scene } = initEngine(this.canvas, './Texture/envWar.jpg')
      this.engine = engine
      this.scene = scene
      // 调试工具
      debuggerLayer(this.scene)
      // 默认天光
      // skyLight(this.scene)
      // 默认相机
      this.camera = defaultCamera(this.scene, this.canvas)

      /* ----------------------以下是测试部分--------------------- */

      // 测试加载
      modelLoadTest(this.scene)
      // 线性动画测试
      // linearAnimationTest(this.scene, this.engine, this.camera)
      // 照片墙测试
      // photoWallDemo(this.canvas)
    }
  }
}
